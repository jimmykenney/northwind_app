# Northwind .NET Full Framework #

Test Application to test some functionalities of Stackify

### Logs ###

This app only uses Stackify's API logs

* DEBUG Log on About page
* ERROR Log on Weather page.
* INFO log on Product details


### Tract Functions ###

* Customers index page
*  Create new customer
*  Delete customer


### Custom Profiling ###

Config file for this project is set as below

` {
    "Class": "Northwind_App.Controllers.ProductsController", 
    "Method": "PrivateProduct" 
} `

* Private Function on Product Details
* Private Function on OrderItem Details w/ DEBUG Log