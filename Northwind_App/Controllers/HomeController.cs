﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Northwind_App.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            StackifyLib.Logger.Queue("ERROR", "Northwind Home\\About");
            return View();
        }

        public ActionResult Weather()
        {
            StackifyLib.Logger.Queue("DEBUG", "Northwind Weather");
            ViewBag.Message = "7 - Day Weather forcast";
            ViewBag.SubMessage = "Widget from OpenWeatherMap.com";

            return View();
        }
    }
}