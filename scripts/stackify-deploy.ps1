﻿Param(

        [Parameter(Mandatory=$True,Position=0)]

        [ValidateSet('complete','start','cancel')]

        [string]$action,

        [Parameter(Mandatory=$True)][string]$apiKey,

        [Parameter(Mandatory=$True)][string]$version,

        [Parameter(Mandatory=$True)][string]$app,

        [Parameter(Mandatory=$True)][string]$env,

        [Parameter(Mandatory=$False)][string]$uri,

        [Parameter(Mandatory=$False)][string]$name,

        [Parameter(Mandatory=$False)][string]$branch,

        [Parameter(Mandatory=$False)][string]$commit,

        [Parameter(Mandatory=$False)][string]$hostApi

)

# build the post url

if (!$hostApi) { $hostApi= 'https://api.stackify.net' }

$post = $hostApi.TrimEnd('/') + '/api/v1/deployments/' + $action

# build the authorization header

$headers = @{'authorization'='ApiKey ' + $apiKey}

# build the body of the post

if (!$name) { $name = $version }

$bodyObj = @{ Version=$version; AppName=$app; EnvironmentName=$env; }

if ($action -eq "start" -or $action -eq "complete"){

        $bodyObj.Name = $name

        if ($uri) { $bodyObj.Uri = $uri }

        if ($branch) { $bodyObj.Branch = $branch }

        if ($commit) { $bodyObj.Commit = $commit }

}

$body = ConvertTo-Json $bodyObj

# send the request

Invoke-WebRequest -Uri $post -Method POST -ContentType "application/json" -Headers $headers -Body $body